use std::{
    fmt::{Display, Write},
    io::{BufRead, Read},
    usize,
};

use error::Error::{self, *};
use std::*;
mod error;

const LEFT_DIAGONAL: [(usize, usize); 3] = [(0, 0), (1, 1), (2, 2)];
const RIGHT_DIAGONAL: [(usize, usize); 3] = [(0, 2), (1, 1), (2, 0)];

impl Display for GameOver {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let out = match self {
            Self::Player1 => "Player 1 has won!",
            Self::Player2 => "Player 2 has won!",
            _ => "The game has tied!",
        };
        f.write_str(out)
    }
}

impl BoardEntry {
    fn is_occupied(&self) -> bool {
        self.value != Empty
    }
}
#[derive(PartialEq)]
enum GameOver {
    Player1,
    Player2,
    Tied,
    NotDone,
}
#[derive(Debug, Clone, PartialEq)]
enum BoardValue {
    Player1,
    Player2,
    Empty,
}

enum Diagonals {
    Left,
    Right,
}

impl Diagonals {
    fn get_indexes(&self) -> [(usize, usize); 3] {
        match self {
            Self::Left => LEFT_DIAGONAL,
            Self::Right => RIGHT_DIAGONAL,
        }
    }
}

impl Default for BoardValue {
    fn default() -> Self {
        Empty
    }
}

#[derive(Debug, Clone)]
struct BoardEntry {
    value: BoardValue,
    min_max: u8,
}

impl Default for BoardEntry {
    fn default() -> Self {
        Self {
            value: BoardValue::default(),
            min_max: 1,
        }
    }
}

impl Display for BoardValue {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let rendered = match self {
            Player1 => 'X',
            Player2 => 'O',
            Empty => '*',
        };
        f.write_char(rendered)
    }
}
impl Display for BoardEntry {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}
impl Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "  1 2 3")?;
        for (row, num) in self.0.iter().zip(1..) {
            write!(f, "{} ", num)?;
            for item in row {
                write!(f, "{} ", item)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl Default for Board {
    fn default() -> Self {
        let rows = [
            BoardEntry::default(),
            BoardEntry::default(),
            BoardEntry::default(),
        ];
        let mut default = Self([rows.clone(), rows.clone(), rows]);
        default.reset_min_max();
        default
    }
}

#[derive(Debug)]
struct Board([[BoardEntry; 3]; 3]);

use BoardValue::*;
impl Board {
    fn ai_play(&mut self) -> Result<(), Error> {
        let mut idx = (0, 0);
        let mut max = 0;
        for (row_idx, row) in self.0.iter().enumerate() {
            for (entry_idx, entry) in row.iter().enumerate() {
                if entry.min_max > max && !self.0[row_idx][entry_idx].is_occupied() {
                    max = entry.min_max;
                    idx = (row_idx, entry_idx);
                }
            }
        }
        self.cast_action(PlayerAction(idx.0, idx.1, Player2))
    }
    fn new() -> Self {
        Self::default()
    }
    fn get_row(&self, row: usize) -> &[BoardEntry; 3] {
        &self.0[row]
    }
    fn get_column(&self, col: usize) -> [&BoardEntry; 3] {
        let data = [&self.0[0][col], &self.0[1][col], &self.0[2][col]];
        data
    }
    fn get_diagonals(&self, row: usize, col: usize) -> Option<Diagonals> {
        let left = [(0, 0), (2, 2)].iter().any(|index| *index == (row, col));
        let right = [(0, 2), (2, 0)].iter().any(|index| *index == (row, col));

        if left {
            Some(Diagonals::Left)
        } else if right {
            Some(Diagonals::Right)
        } else {
            None
        }
    }
    fn diagonal_min_max(&self, row: usize, col: usize) -> u8 {
        let diagonals = self.get_diagonals(row, col);
        let tiles = match diagonals {
            Some(val) => val.get_indexes(),
            None => return 0,
        };
        let ai_count = tiles
            .iter()
            .filter(|(row, col)| self.0[*row][*col].value == Player2)
            .count();
        let opponent_count = tiles
            .iter()
            .filter(|(row, col)| self.0[*row][*col].value == Player1)
            .count();
        if ai_count == 2 {
            85
        } else if opponent_count == 2 {
            42
        } else if opponent_count == 0 {
            ai_count as u8
        } else {
            opponent_count as u8
        }
    }
    fn column_min_max(&self, col: usize) -> u8 {
        let col = self.get_column(col);
        let opponent_count = col.iter().filter(|item| item.value == Player1).count();
        let ai_count = col.iter().filter(|item| item.value == Player2).count();

        if ai_count == 2 {
            85
        } else if opponent_count == 2 {
            42
        } else if opponent_count == 0 {
            ai_count as u8
        } else {
            opponent_count as u8
        }
    }
    fn row_min_max(&self, row: usize) -> u8 {
        let row = self.get_row(row);

        let ai_count = row.iter().filter(|item| item.value == Player2).count();
        let opponent_count = row.iter().filter(|item| item.value == Player1).count();
        if ai_count == 2 {
            85
        } else if opponent_count == 2 {
            42
        } else if opponent_count == 0 {
            ai_count as u8
        } else {
            opponent_count as u8
        }
    }

    fn set_min_max(&mut self, row: usize, col: usize) {
        let min_max =
            self.row_min_max(row) + self.column_min_max(col) + self.diagonal_min_max(col, row);
        let new = if self.0[row][col].value != Empty {
            0
        } else {
            self.0[row][col].min_max + min_max
        };
        let get = &mut self.0[row][col];
        get.min_max = new;
    }
    fn update_min_max(&mut self) -> Result<(), Error> {
        self.reset_min_max();
        for row in 0..3 {
            for col in 0..3 {
                self.set_min_max(row, col);
            }
        }
        Ok(())
    }
    fn reset_min_max(&mut self) {
        for row in &mut self.0 {
            for entry in row {
                entry.min_max = 1; // every position has 2 possible win conditions; use 1 as a base
            }
        }
        for (row, col) in &[(0, 0), (0, 2), (2, 0), (2, 2)] {
            self.0[*row][*col].min_max += 1; // every corner piece has 3 possible win conditions; use the difference between the base
        }
        self.0[1][1].min_max += 3; /*
                                      the center piece has 4 possible win conditions;
                                      but we shall weigh it the difference between the base plus one more
                                      because obtaining this spot is necessary as it avoids a bruteforce victory
                                   */
    }

    fn cast_action(&mut self, action: PlayerAction) -> Result<(), Error> {
        let get = &mut self.0[action.0][action.1];
        get.value = action.2;
        self.update_min_max()
    }
    fn draw_min_max_map(&self) {
        for row in &self.0 {
            for entry in row {
                print!("{} ", entry.min_max);
            }
            println!();
        }
    }
    fn horizontal_victory(&self, player: &BoardValue) -> bool {
        self.0
            .iter()
            .any(|row| row.iter().all(|item| item.value == *player))
    }
    fn diagonal_victory(&self, player: &BoardValue) -> bool {
        [LEFT_DIAGONAL, RIGHT_DIAGONAL].iter().any(|diagonal| {
            diagonal
                .iter()
                .all(|(row, col)| self.0[*row][*col].value == *player)
        })
    }
    fn vertical_victory(&self, player: &BoardValue) -> bool {
        (0..3).any(|column| {
            self.get_column(column)
                .iter()
                .all(|entry| entry.value == *player)
        })
    }
    fn player_wins(&self, player: BoardValue) -> bool {
        self.vertical_victory(&player)
            || self.horizontal_victory(&player)
            || self.diagonal_victory(&player)
    }
    fn game_over(&self) -> GameOver {
        if self.player_wins(Player1) {
            GameOver::Player1
        } else if self.player_wins(Player2) {
            GameOver::Player2
        } else if self.board_is_filled() {
            GameOver::Tied
        } else {
            GameOver::NotDone
        }
    }
    fn board_is_filled(&self) -> bool {
        self.0.iter().flatten().all(|item| item.value != Empty)
    }
}

struct PlayerAction(usize, usize, BoardValue);

fn input_loop<T: Read + BufRead>(stdin: &mut T, board: &Board) -> (usize, usize) {
    loop {
        match validate_input(stdin, board) {
            Err(err) => println!("{}", err),
            Ok(ok) => return ok,
        }
    }
}

fn validate_input<T: BufRead + Read>(
    reader: &mut T,
    board: &Board,
) -> Result<(usize, usize), Error> {
    let mut input = String::new();
    reader.read_line(&mut input)?;
    let mut iter = input.split_whitespace();
    let row = iter.next().ok_or(InvalidInput)?.parse()?;
    let col = iter.next().ok_or(InvalidInput)?.parse()?;

    if !(matches!(row, 1..=3) && matches!(col, 1..=3)) {
        return Err(InvalidIndex);
    }

    if board.0[row - 1][col - 1].is_occupied() {
        return Err(SpaceOccupied);
    }

    Ok((row - 1, col - 1))
}

fn game_loop() -> Result<(), Error> {
    let mut player_toggle = true;
    let mut board = Board::new();
    let stdin = io::stdin();
    let mut stdin = stdin.lock();
    let mut stdout = io::stdout();
    loop {
        println!("\n{}", board);
        if board.game_over() != GameOver::NotDone {
            break;
        } else if !player_toggle {
            board.ai_play()?;
            player_toggle = true;
            continue;
        };
        // board.draw_min_max_map();
        print!("Player 1: ");
        io::Write::flush(&mut stdout)?;
        let input = input_loop(&mut stdin, &board);
        let out = PlayerAction(
            input.0,
            input.1,
            if player_toggle { Player1 } else { Player2 },
        );
        board.cast_action(out)?;
        board.draw_min_max_map();
        player_toggle = false;
    }
    println!("{}", board.game_over());
    Ok(())
}

fn main() {
    game_loop().expect("Failed to initialize game loop.");
}
