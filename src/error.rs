use std::{fmt::Display, *};
use Error::*;
#[derive(Debug)]
pub(crate) enum Error {
    InvalidIndex,
    SpaceOccupied,
    InvalidInput,
    IO(io::Error),
    ParseInt(num::ParseIntError),
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            InvalidIndex => write!(f, "invalid index given"),
            InvalidInput => write!(f, "invalid input given"),
            SpaceOccupied => write!(f, "selected space is occupied"),
            IO(err) => write!(f, "{}", err),
            ParseInt(err) => write!(f, "{}", err),
        }
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        IO(e)
    }
}

impl From<num::ParseIntError> for Error {
    fn from(e: num::ParseIntError) -> Self {
        ParseInt(e)
    }
}
